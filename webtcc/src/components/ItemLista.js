import React from "react";
// import { ClienteContext } from "./ClienteContext";
import "./ItemLista.css"

const ItemLista = (props) => {

    return (
        <div className="media my-3">

            <img className="img-top" src={props.foto} alt="Coletoras Encontradas" />
            <div className="ml-3">
                <h4 className="title">Coletora: { }
                    {props.nome_coletora}
                </h4>
                <p className="title">Endereço: { }
                    {props.endereco_coletora}, { }
                
                    {props.numero_coletora}
                </p>
                <p className="title">Bairro: { }
                    {props.bairro_coletora}
                </p>
                <p className="title">Telefone: { }
                    {props.telefone}
                </p>
                <input
                        // type="submit"
                        value="Agendar"
                        className="btn btn-success"
                    />
            </div>

        </div>
    );
};

export default ItemLista;